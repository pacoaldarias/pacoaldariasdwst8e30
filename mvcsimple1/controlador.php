
<?php

include ("vista.php");
include ("modelo.php");

mostrarCabecera();
mostrarFormulario();

if (isset($_REQUEST["nombre"])) {
  if ($_REQUEST["nombre"] != "") {
    $modelo = new modelo($_REQUEST["nombre"]);
    mostrarNombre($modelo->getNombre());
  }
}
mostrarPie();
?>

