<?php ob_start() ?>

<h1><?php echo $params->getNombre(); ?></h1>
<table border="1">

    <tr>
        <td>Energía</td>
        <td><?php echo $alimento->getEnergia(); ?></td>

    </tr>
    <tr>
        <td>Proteina</td>
        <td><?php echo $alimento->getProteina() ?></td>

    </tr>
    <tr>
        <td>Hidratos de Carbono</td>
        <td><?php echo $alimento->getHidratocarbono(); ?></td>

    </tr>
    <tr>
        <td>Fibra</td>
        <td><?php echo $alimento->getFibra(); ?></td>

    </tr>
    <tr>
        <td>Grasa total</td>
        <td><?php echo $alimento->getGrasatotal(); ?></td>

    </tr>

</table>


<?php $contenido = ob_get_clean() ?>

<?php include 'alimentoslayout.php' ?>
