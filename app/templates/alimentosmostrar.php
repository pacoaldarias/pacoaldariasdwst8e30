<?php
ob_start()
?>
<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>


<table>
    <tr>
        <th>alimento (por 100g)</th>
        <th>energía (Kcal)</th>
        <th>grasa (g)</th>
    </tr>
    <?php echo "Mostrando Alimentos"; ?>
    <?php foreach ($params['alimentos'] as $alimento) : ?>
      <tr>
          <td><a href="index.php?ctl=alimentosver&id=<?php echo $alimento->getId(); ?>">
                  <?php echo $alimento->getNombre(); ?></a></td>
          <td><?php echo $alimento->getEnergia(); ?></td>
          <td><?php echo $alimento->getGrasatotal(); ?></td>
      </tr>
    <?php endforeach; ?>

</table>

<?php $contenido = ob_get_clean() ?>

<?php include 'alimentoslayout.php' ?>
