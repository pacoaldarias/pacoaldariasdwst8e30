<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

class Controller {

  public function inicio() {
    $params = array(
      'mensaje' => 'Bienvenido al repositorio de alimentos',
      'fecha' => date('d-m-y'),
    );
    require __DIR__ . '/templates/inicio.php';
  }

  public function alimentos() {
    $params = array(
      'mensaje' => 'Alimentos',
      'fecha' => date('d-m-y'),
    );
    require __DIR__ . '/templates/alimentosinicio.php';
  }

}

?>
